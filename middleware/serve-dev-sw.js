import { readFileSync } from 'node:fs';

const cacheUrl = new URL('../public/cache.js', import.meta.url);

const devSwContent = readFileSync(cacheUrl, 'utf-8')
		.replace('const PRODUCTION_MODE = true;', 'const PRODUCTION_MODE = false;');

export async function serveDevSw(ctx, next) {
	if (ctx.method !== "GET" || ctx.url !== '/cache.js') {
		return next();
	}
	ctx.set('content-type', 'application/javascript; charset=utf-8');
	ctx.body = devSwContent;
}
