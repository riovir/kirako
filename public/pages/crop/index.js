import '../play/orientable-image.js';
import { css } from '../../core/index.js';
import { DEFAULT_IMAGE } from '../setup/index.js';

export function setup({ playground, storage }) {
	playground.innerHTML = /* html */`
		<orientable-image id="image"></orientable-image>

		<label id="cropping-feedback"></label>

		<label class="range-input">
			From X <input type="range" value="0" min="0" max="1" step="0.01">
		</label>
		<label class="range-input">
			To X<input type="range" value="1" min="0" max="1" step="0.01">
		</label>
		<label class="range-input">
			From Y<input type="range" value="0" min="0" max="1" step="0.01">
		</label>
		<label class="range-input">
			To Y<input type="range" value="1" min="0" max="1" step="0.01">
		</label>
	`;

	const image = playground.querySelector('#image');
	const label = playground.querySelector('#cropping-feedback');
	const [inputFromX, inputToX, inputFromY, inputToY] = playground.querySelectorAll('input[type=range]');

	const src = storage.currentImage || DEFAULT_IMAGE;
	const cropping = (storage.croppings || {})[src];
	Object.assign(image, { src, cropping });
	const { fromX, fromY, toX, toY } = image.cropping;
	inputFromX.value = fromX;
	inputFromY.value = fromY;
	inputToX.value = toX;
	inputToY.value = toY;

	const crop = ({ prop }) => ({ target }) => {
		image.cropping = { ...image.cropping, [prop]: target.value };
		storage.croppings = { ...storage.croppings, [src]: image.cropping };
		label.textContent = JSON.stringify(image.cropping)
			.replaceAll('"', '')
			.replace(',', ' ')
			.replace('{', '')
			.replace('}', '');
	};
	inputFromX.addEventListener('input', crop({ prop: 'fromX'}));
	inputFromY.addEventListener('input', crop({ prop: 'fromY'}));
	inputToX.addEventListener('input', crop({ prop: 'toX'}));
	inputToY.addEventListener('input', crop({ prop: 'toY'}));
}

export const styles = css`
	#playground {
		gap: var(--spacer-6x);
	}

	#image {
		outline: 1px solid var(--color-border-primary-default);
		pointer-events: all;
	}

	.range-input {
		display: grid;
		grid-template-columns: 4em 1fr;
		gap: var(--spacer-2x);
	}
`;
