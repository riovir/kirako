import './moveable-group.js';
import './piece-drawer.js';
import './puzzle-piece.js';
import './puzzle-board.js';
import { DEFAULT_IMAGE, rememberImage } from '../setup/index.js';
import { preload } from './orientable-image.js';
import { applyLocation, reflectLocation } from './puzzle-state.js';
import { randomThunk } from './random.js';
import { Interactions, isSolved } from './interactions.js';
import { maxSizeOf } from './jigsaw-cutout.js';
import { complement, filter, find } from './utils.js';
import { isInDrawer } from './piece-data.js';

export async function setup({ navbar, playground, params, storage }) {
	if (screen.orientation) { screen.orientation.onchange = location.reload.bind(location); }

	ensureChild(navbar, ToggleFocusButton({ playground }));

	playground.innerHTML = /* html */`
		<div id="puzzle-container" class="puzzle-container"></div>
		<audio id="click" src="assets/click.mp3" preload="auto"></audio>
	`;

	const rotate = params.rotate ?? storage.rotate ?? false;
	const seed = params.seed || Date.now();
	params.seed = seed;

	const src = params.src || DEFAULT_IMAGE;
	const cropping = (storage.croppings || {})[src];
	const piecesX = parseInt(params.piecesX) || 5;
	const piecesY = parseInt(params.piecesY) || 5;
	const orientation = params.orientPuzzle || null;

	await preload({ src });

	const container = playground.querySelector('#puzzle-container');
	const pieceDrawer = document.createElement('piece-drawer');
	container.appendChild(pieceDrawer);
	pieceDrawer.active = false;

	const board = document.createElement('puzzle-board');
	board.style.opacity = 0;
	Object.assign(board, { orientation, src, cropping, piecesX, piecesY, seed });
	container.appendChild(board);
	await board.updateComplete;

	const clickSound = playground.querySelector('#click');
	const playClickSound = () => clickSound.play();

	const pieces = board.querySelectorAll('moveable-group');

	const random = randomThunk(params);
	const reflect = reflectLocation({ params, board });

	const interactions = Interactions({ random, board, pieces, pieceDrawer, reflect, playClickSound });
	const { drawerAdd, scrambleBoard, onMove, onMoveEnd, onTapRotate, onDrawerMove } = interactions;

	window.onpopstate = () => applyLocation({ board, params, drawerAdd });

	board.addEventListener('src-changed', ({ target }) => params.src = target.src);
	board.addEventListener('ready', rememberImage({ storage }));
	board.addEventListener('ready', async () => {
		applyLocation({ board, params, drawerAdd });

		if (params.scramble ?? false) {
			params.scramble = null;
			await scrambleBoard({ rotate });
		}
		board.style.opacity = null;
		if (!find(piece => !isSolved({ piece }), pieces)) { return; }
		setTimeout(() => {
			for (const target of filter(complement(isInDrawer), pieces)) {
				onMove({ target });
				onMoveEnd({ target, sound: false });
			}
		}, 50);
	});
	board.addEventListener('move', onMove);
	board.addEventListener('move-end', onMoveEnd);
	if (rotate) {
		board.addEventListener('tap', onTapRotate);
	}

	board.addEventListener('ready', () => {
		const { pieceHeight, pieceWidth } = board.querySelector('moveable-group').dataset;
		const pieceSize = Math.max(Number(pieceHeight), Number(pieceWidth));
		pieceDrawer.style.setProperty('--piece-drawer-size', `${maxSizeOf(pieceSize) + 32}px`);
	});
	pieceDrawer.addEventListener('move', onDrawerMove);
}

function ToggleFocusButton({ playground }) {
	const button = document.createElement('kr-button');
	button.id = 'focus';
	button.setAttribute('slot', 'brand');
	button.setAttribute('compact', '');
	button.setAttribute('minimal', '');
	button.setAttribute('aria-label', 'Focus on unfinished pieces');
	button.innerHTML = /* html */`
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6 8" focusable="false" aria-hidden="true">
			<path fill="currentColor" d="M 0 3 a 1 1 0 0 1 6 0 l 0 5 a 1 1 0 0 0 -2 0 a 1 1 0 0 0 -2 0 a 1 1 0 0 0 -2 0 z M 0.7 3 a 1 1 0 1 0 2 0 a 1 1 0 1 0 -2 0 M 3.3 3 a 1 1 0 1 0 2 0 a 1 1 0 1 0 -2 0" />
		</svg>
	`;
	button.addEventListener('click', () => {
		playground.classList.toggle('has-focus-on-unfinished');
		button.classList.toggle('is-active');
		button.blur();
	});
	return button;
}

function ensureChild(parent, child) {
	if (parent.querySelector(`#${child.id}`)) { return; }
	parent.appendChild(child);
}
