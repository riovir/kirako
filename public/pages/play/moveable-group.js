const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		box-sizing: border-box;
		display: inline-block;
		pointer-events: none;
		z-index: 2;
		-webkit-tap-highlight-color: transparent;
		-webkit-touch-callout: none;
		-webkit-user-select: none;
		user-select: none;
	}

	#rotator {
		width: 100%;
		height: 100%;
	}

	::slotted(*) {
		pointer-events: all;
	}

	:host([disabled]) {
		z-index: 0;
		pointer-events: none;
	}

	:host(:not([disabled]):hover) {
		z-index: var(--z-topmost);
	}
	@media (hover: hover) {
		:host(:not([disabled]):hover) {
			filter: brightness(125%);
		}
	}
</style>
<div id="rotator">
	<slot></slot>
</div>
`;

function onGlobalPointerMove(event) {
	event.preventDefault();
	if (!State.activePiece || event.pointerType === 'touch') { return; }
	State.activePiece._moveUpdate(event);
	if (!event.buttons && event.pointerType === 'mouse') {
		State.activePiece._moveEnd(event);
	}
}

function onGlobalPointerUp(event) {
	if (!State.activePiece || event.pointerType === 'touch') { return; }
	State.activePiece._moveEnd(event);
}

const State = {
	activePiece: null,
};

class MoveableGroup extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this._offsetX = 0;
		this._offsetY = 0;
		this._rotation = 0;
		this._target = this;

		this._updateLocation = this._updateLocation.bind(this);
		this._moveStart = this._moveStart.bind(this);
		this._moveUpdate = this._moveUpdate.bind(this);
		this._moveEnd = this._moveEnd.bind(this);
		this._hasMoved = false;

		this.move = this.move.bind(this);
		this._move = this._move.bind(this);
		this.rotate = this.rotate.bind(this);
		this._rotate = this._rotate.bind(this);
		this._rebaseTransformOrigin = this._rebaseTransformOrigin.bind(this);

		this._rotateFinished = Promise.resolve();
		this._moveFinished = Promise.resolve();
		this._rotatorElement = shadowRoot.querySelector('#rotator');
		this._rotatorElement.addEventListener('transitionend', this._removeRotatorTransition);

		this.addEventListener('pointerdown', this._moveStart);

		/* Touch-only */
		this.addEventListener('touchmove', ({ touches }) => this._moveUpdate(touches[0]));
		this.addEventListener('touchend', ({ touches }) => this._moveEnd(touches[0]));
		this.addEventListener('touchcancel', ({ touches }) => this._moveEnd(touches[0]));

		/* Non-touch */
		window.addEventListener('pointermove', onGlobalPointerMove);
		this.addEventListener('pointerup', onGlobalPointerUp);
	}

	get offsetX() {
		return this._offsetX;
	}
	set offsetX(value) {
		this._offsetX = value;
		this._updateLocation();
	}

	get offsetY() {
		return this._offsetY;
	}
	set offsetY(value) {
		this._offsetY = value;
		this._updateLocation();
	}

	async move({ offsetX = this._offsetX, offsetY = this._offsetY, duration = 100 }) {
		if (this.offsetX === offsetX && this.offsetY === offsetY) { return; }
		await this._moveFinished;
		this._moveFinished = this._move({ offsetX, offsetY, duration });
		await this._moveFinished;
	}

	async _move({ offsetX, offsetY, duration }) {
		const options = { duration, easing: 'ease-in-out' };
		const transform = [
			`translate(${this._offsetX}px, ${this._offsetY}px)`,
			`translate(${offsetX}px, ${offsetY}px)`,
		];
		await this._target.animate({ transform }, options).finished;
		this._offsetX = offsetX;
		this._offsetY = offsetY;
		this._updateLocation();
	}

	async rotate(deg = 90, { around, animate = true } = {}) {
		if (deg === 0) { return; }
		await this._rotateFinished;
		this._rotateFinished = this._rotate(deg, { around, animate });
		await this._rotateFinished;
	}

	async _rotate(deg, { around, animate }) {
		const duration = animate ? 100 : 0;
		const options = { duration, fill: 'forwards', easing: 'ease-in-out' };
		const transform = [
			`rotate(${this.rotation}deg)`,
			`rotate(${this.rotation + deg}deg)`
		];
		const origin = this.transformOrigin;
		if (around) { this._rebaseTransformOrigin(around); }
		await this._rotatorElement.animate({ transform }, options).finished;
		this.rotation = (this.rotation + deg) % 360;
		if (around) { this._rebaseTransformOrigin(origin); }
	}

	get rotation() {
		return this._rotation;
	}
	set rotation(value) {
		this._rotation = value;
		this._updateLocation();
		this.dispatchEvent(new Event('rotation-changed', { bubbles: true, composed: true }));
	}

	_rebaseTransformOrigin(value) {
		const { x: x1, y: y1 } = this._rotatorElement.getBoundingClientRect();
		this.transformOrigin = value;
		const { x: x2, y: y2 } = this._rotatorElement.getBoundingClientRect();
		const xDiff = Math.round(x1 - x2);
		const yDiff = Math.round(y1 - y2);
		this._offsetX += xDiff;
		this._offsetY += yDiff;
		this._updateLocation();
	}

	get transformOrigin() {
		return getComputedStyle(this._rotatorElement).transformOrigin;
	}
	set transformOrigin(value = null) {
		return this._rotatorElement.style.transformOrigin = value;
	}

	get target() {
		return this._target;
	}
	set target(value = null) {
		return this._target = value;
	}

	async _moveStart({ screenX, screenY }) {
		await this._rotateFinished;
		this._hasMoved = false;
		if (this.hasAttribute('disabled')) { return; }
		State.activePiece = this;
		const { offsetX, offsetY } = this;
		this._moveBasis = { screenX, screenY, offsetX, offsetY };
		this.style.zIndex = 999;
		this.dispatchEvent(new Event('move-start', { bubbles: true, composed: true }));
	}

	_moveUpdate({ screenX: x, screenY: y }) {
		if (State.activePiece !== this || !this._moveBasis) { return; }
		const { screenX, screenY, offsetX, offsetY } = this._moveBasis;
		if (!isWithin({ distance: 5, x1: x, y1: y, x2: screenX, y2: screenY })) {
			this._hasMoved = true;
		}
		this._offsetX = x - screenX + offsetX;
		this._offsetY = y - screenY + offsetY;
		this._updateLocation();
		this.dispatchEvent(new Event('move', { bubbles: true, composed: true }));
	}

	_moveEnd() {
		State.activePiece = null;
		this._moveBasis = null;
		this.style.zIndex = null;
		if (this.hasAttribute('disabled')) { return; }
		const event = this._hasMoved ? 'move-end' : 'tap';
		this.dispatchEvent(new Event(event, { bubbles: true, composed: true }));
	}

	_updateLocation() {
		this._offsetX = Math.round(this._offsetX);
		this._offsetY = Math.round(this._offsetY);
		this.target.style.transform = `translate(${this._offsetX}px, ${this._offsetY}px)`;
		this._rotatorElement.style.transform = `rotate(${this._rotation}deg)`;
	}
}

function isWithin({ distance, x1, y1, x2, y2 }) {
	return Math.abs(x1 - x2) <= distance && Math.abs(y1 - y2) <= distance;
}

customElements.define('moveable-group', MoveableGroup);
