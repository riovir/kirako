/** Using the Fisher-Yates algorithm */
export function shuffle(array) {
	const result = [...array];
	let index = result.length;
	while (--index > 0) {
		const indexRandom = Math.floor(Math.random() * (index + 1));
		[result[indexRandom], result[index]] = [result[index], result[indexRandom]];
	}
	return result;
}

/** Used as https://ramdajs.com/docs/#always */
export function always(value) {
	return () => value;
}

/** Used as https://ramdajs.com/docs/#complement */
export function complement(predicate) {
	return candidate => !predicate(candidate);
}

/** Used as https://ramdajs.com/docs/#filter */
export function filter(predicate, array) {
	const result = [];
	for (const element of array) {
		if (predicate(element)) {
			result.push(element);
		}
	}
	return result;
}

/** Used as https://ramdajs.com/docs/#find */
export function find(predicate, array) {
	for(const element of array) {
		if (predicate(element)) {
			return element;
		}
	}
}

/** Used as https://ramdajs.com/docs/#findIndex */
export function findIndex(predicate, array) {
	if (!array.length) { return -1; }
	for(let index = 0; index < array.length; index++) {
		if (predicate(array[index])) {
			return index;
		}
	}
	return -1;
}
/** Used as https://ramdajs.com/docs/#min */
export function min(a, b) {
	return Math.min(a, b);
}

/** Used as https://ramdajs.com/docs/#map */
export function map(mapper, array) {
	const result = [];
	for (const element of array) {
		result.push(mapper(element));
	}
	return result;
}

/** Used as https://ramdajs.com/docs/#min */
export function max(a, b) {
	return Math.max(a, b);
}

/** Used as https://ramdajs.com/docs/#pipe */
export function partition(predicate, array) {
	const matches = [];
	const rest = [];
	for (const element of array) {
		const bucket = predicate(element) ? matches : rest;
		bucket.push(element);
	}
	return [matches, rest];
}

/** Used as https://ramdajs.com/docs/#pipe */
export function pipe(firstFn, ...fns) {
	return (...args) => fns.reduce((acc, fn) => fn(acc), firstFn(...args));
}

/** Used as https://ramdajs.com/docs/#prop */
export function prop(name) {
	return object => object[name];
}

/** Used as https://ramdajs.com/docs/#without */
export function without(elements, array) {
	return array.filter(element => !elements.includes(element));
}
