import { useUpdate } from './use-update.js';

test('delays running updates to future event loop cycles', () => {
	let callCount = 0;
	const update = () => { callCount++; };
	const { requestUpdate } = useUpdate({ update });
	requestUpdate();
	requestUpdate();
	expect(callCount).toBe(0);
});

test('requestUpdate returns the complete promise', async () => {
	let callCount = 0;
	const update = () => { callCount++; };
	const { complete, requestUpdate } = useUpdate({ update });
	requestUpdate();
	requestUpdate();
	await complete.value;
	expect(callCount).toBe(1);
	requestUpdate();
	requestUpdate();
	await requestUpdate();
	expect(callCount).toBe(2);
});

test('batches together updates to be run together', async () => {
	let callCount = 0;
	const update = () => { callCount++; };
	const { requestUpdate } = useUpdate({ update });
	requestUpdate();
	await requestUpdate();
	expect(callCount).toBe(1);
});

test('multiple update cycles keep working', async () => {
	let callCount = 0;
	const update = () => { callCount++; };
	const { complete, requestUpdate } = useUpdate({ update });
	requestUpdate();
	requestUpdate();
	await complete.value;
	expect(callCount).toBe(1);
	requestUpdate();
	requestUpdate();
	await complete.value;
	expect(callCount).toBe(2);
});

test('supports async update hook', async () => {
	let callCount = 0;
	const update = async () => {
		await wait({ msec: 5 });
		callCount++;
	};
	const { requestUpdate } = useUpdate({ update });
	requestUpdate();
	requestUpdate();
	await requestUpdate();
	expect(callCount).toBe(1);
});

test('schedules requestUpdate after the one that is still running', async () => {
	let callCount = 0;
	let prop = null;
	let current = 'initial';
	const update = async () => {
		await wait({ msec: 8 });
		callCount++;
		current = prop;
	};
	const { requestUpdate } = useUpdate({ update });
	prop = 'first';
	const completedFirst = requestUpdate();
	expect(current).toBe('initial');

	await wait({ msec: 4 }); // The first props is still not yet read
	prop = 'second';
	const completedSecond = requestUpdate();
	expect(current).toBe('initial');

	await completedFirst;
	expect(current).toBe('second');
	expect(callCount).toBe(1);

	await completedSecond;
	expect(current).toBe('second');
	expect(callCount).toBe(2);
});

test('keeps track of updated props', async () => {
	let called = false;
	const update = changedProps => {
		called = true;
		expect(changedProps.has('x')).toBe(true);
		expect(changedProps.get('x')).toBe(1);
		expect(changedProps.has('y')).toBe(true);
		expect(changedProps.get('y')).toBe(1);
	};
	const { requestUpdate } = useUpdate({ update });
	requestUpdate('x', 1);
	await requestUpdate('y', 1);
	expect(called).toBe(true);
});

test('clears updated props after update', async () => {
	let callCount = 0;
	const update = changedProps => {
		callCount++;
		if (callCount < 2) { return; }
		expect(changedProps.has('x')).toBe(false);
		expect(changedProps.has('y')).toBe(false);
		expect(changedProps.get('z')).toBe(1);
	};
	const { requestUpdate } = useUpdate({ update });
	requestUpdate('x', 1);
	await requestUpdate('y', 1);
	await requestUpdate('z', 1);
	expect(callCount).toBe(2);
});

function wait({ msec }) {
	return new Promise(resolve => setTimeout(resolve, msec));
}
