const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		display: block;
		aspect-ratio: var(--aspect-ratio, 1);
		min-height: 32px;
		max-height: 100%;
		max-width: 100%;
		position: relative;
		pointer-events: none;
		overflow: hidden;
	}
	#image {
		display: block;
		height: var(--height, 100%);
		transform-origin: top left;
		translate: var(--img-offset-x, 0) var(--img-offset-y, 0);
	}
</style>
<img id="image" rel="preload">
`;

class CroppableImage extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this.updated = Promise.resolve('blank');

		this._update = this._update.bind(this);
		this._updateSrc = this._updateSrc.bind(this);

		this._imageElement = shadowRoot.querySelector('#image');

		this._imageElement.addEventListener('load', () => {
			this.dispatchEvent(new Event('load', { bubbles: true, composed: true }));
		});
		this._observer = new ResizeObserver(this._update);
	}

	connectedCallback() {
		upgradeProperty(this, 'src');

		if (this.hasAttribute('src')) {
			this.src = this.getAttribute('src');
		}
		this._observer.observe(this);
		this._update();
	}

	get src() {
		return this._imageElement.src;
	}
	set src(value) {
		this.setAttribute('src', value);
		this._updateSrc(value).then(this._update);
	}

	async _updateSrc(src) {
		if (!src) { return; }
		this.updated = new Promise(resolve => {
			this._imageElement.onload = resolve;
		});
		this._imageElement.src = src;
		return this.updated;
	}

	get naturalWidth() {
		return this._imageElement.naturalWidth * (this.cropToX - this.cropFromX);
	};
	get naturalHeight() {
		return this._imageElement.naturalHeight * (this.cropToY - this.cropFromY);
	};

	get cropping() {
		const fromX = parseFloat(this.style.getPropertyValue('--crop-from-x') || '0');
		const fromY = parseFloat(this.style.getPropertyValue('--crop-from-y') || '0');
		const toX = parseFloat(this.style.getPropertyValue('--crop-to-x') || '1');
		const toY = parseFloat(this.style.getPropertyValue('--crop-to-y') || '1');
		return { fromX, fromY, toX, toY };
	}
	set cropping(value) {
		const { fromX, toX, fromY, toY } = Cropping(value);
		this.style.setProperty('--crop-from-x', fromX);
		this.style.setProperty('--crop-from-y', fromY);
		this.style.setProperty('--crop-to-x', toX);
		this.style.setProperty('--crop-to-y', toY);
		this._update();
	}
	get cropFromX() { return this.cropping.fromX; }
	get cropFromY() { return this.cropping.fromY; }
	get cropToX() { return this.cropping.toX; }
	get cropToY() { return this.cropping.toY; }

	_update() {
		const diffX = Math.max(this.cropToX - this.cropFromX, 0.001);
		const diffY = Math.max(this.cropToY - this.cropFromY, 0.001);

		this.style.setProperty('--aspect-ratio', `${this.naturalWidth} / ${this.naturalHeight}`);

		this._imageElement.style.setProperty('--width', `${100 / diffX}%`);
		this._imageElement.style.setProperty('--height', `${100 / diffY}%`);

		const offsetX = -this.cropFromX;
		const offsetY = -this.cropFromY;
		this._updateImgProps({ offsetX, offsetY });
		this.dispatchEvent(new Event('updated'));
	}

	_updateImgProps({ offsetX = 0, offsetY = 0, scale = 1 } = {}) {
		this._imageElement.style.setProperty('--img-offset-x', `${offsetX * 100}%`);
		this._imageElement.style.setProperty('--img-offset-y', `${offsetY * 100}%`);
		this._imageElement.style.setProperty('--img-scale', scale);
	}
}

function Cropping(value = {}) {
	const maybeValid = value => 0 <= value && value <= 1 ? value : null;
	const _fromX = maybeValid(value.fromX) ?? 0;
	const _fromY = maybeValid(value.fromY) ?? 0;
	const _toX = maybeValid(value.toX) ?? 1;
	const _toY = maybeValid(value.toY) ?? 1;
	const [fromX, toX] = [_fromX, _toX].sort();
	const [fromY, toY] = [_fromY, _toY].sort();
	return { fromX, fromY, toX, toY };
}

function upgradeProperty(host, prop) {
	if (Object.hasOwn(host, prop)) {
	  const value = host[prop];
	  delete host[prop];
	  host[prop] = value;
	}
  }

customElements.define('croppable-image', CroppableImage);
