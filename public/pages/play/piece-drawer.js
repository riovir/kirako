import './moveable-group.js';

const HANDLE_SIZE_PX = 20;
const TRANSITION_SPEED = 150;

const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		--spacer-scrollbar-width: var(--spacer-4x);
		--piece-drawer-gradient-start: 50%;
		box-sizing: border-box;
		position: absolute;
		display: grid;
		grid-template-columns: 1fr ${HANDLE_SIZE_PX}px;
		z-index: 1;
		height: var(--piece-drawer-size, 0);
		background: linear-gradient(90deg,
			var(--color-bg-glass-default) 0%,
			var(--color-bg-glass-default) var(--piece-drawer-gradient-start),
			var(--color-fg-primary-soft) 100%);
		border-radius: var(--spacer-radius);
		overflow: hidden;
		left: 0;
		right: 0;
		bottom: 0;
	}

	:host([hidden]) {
		display: none;
	}

	moveable-group {
		background: var(--color-fg-primary-soft);
		pointer-events: all;
		cursor: grab;
		width: 100%;
		height: 100%;
		box-shadow: -5px 0px 5px -2px rgb(0 0 0 / 35%)
	}

	.wrapper {
		width: 100%;
		height: 100%;
	}

	#content {
		width: max(var(--piece-drawer-length, 9999px), 100%);
		height: 100%;
	}

	.is-scrollable {
		max-height: 100%;
		max-width: 100%;
		overflow: auto;
		overscroll-behavior: contain;
	}
	.is-scrollable::-webkit-scrollbar {
		width: var(--spacer-scrollbar-width);
		height: var(--spacer-scrollbar-width);
		background-color: var(--color-scrollbar-bg);
	}
	.is-scrollable::-webkit-scrollbar-corner {
		background-color: var(--color-scrollbar-bg);
	}
	.is-scrollable::-webkit-scrollbar-thumb {
		background: var(--color-scrollbar-thumb);
	}

	@media (orientation: landscape) {
		:host {
			left: auto;
			top: 0;
			bottom: 0;
			right: 0;
			height: auto;
			width: var(--piece-drawer-size, 0);
			grid-template-columns: 1fr;
			grid-template-rows: 1fr ${HANDLE_SIZE_PX}px;
			background: linear-gradient(180deg,
				var(--color-bg-glass-default) 0%,
				var(--color-bg-glass-default) var(--piece-drawer-gradient-start),
				var(--color-fg-primary-soft) 100%);
		}

		moveable-group {
			box-shadow: 0px -5px 5px -2px rgb(0 0 0 / 35%)
		}

		#content {
			/* The + 1px prevents an undesired pull-down-to-refresh effect */
			height: max(var(--piece-drawer-length, 9999px), calc(100% + 1px));
			width: 100%;
		}
	}

	@media (pointer: coarse) {
		:host {
			height: calc(var(--piece-drawer-size, 100px) + 24px);
		}
		.is-scrollable {
			scrollbar-width: none;
		}
		.is-scrollable::-webkit-scrollbar {
			display: none;
		}
		@media (orientation: landscape) {
			:host {
				height: auto;
				width: calc(var(--piece-drawer-size, 100px) + 24px);
			}
		}
	}
</style>
<div id="scrollable-group" class="wrapper is-scrollable">
	<div id="content"></div>
</div>
<moveable-group></moveable-group>
`;

class PieceDrawer extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this._isPortrait = true;
		this._active = false;
		this._offsetXLast = 0;
		this._offsetYLast = 0;
		this._scrollTopLast = 0;
		this._scrollLeftLast = 0;

		this._toggleFinished = Promise.resolve();
		this._captureOffset = this._captureOffset.bind(this);
		this._dispatchMove = this._dispatchMove.bind(this);
		this._captureOffset = this._captureOffset.bind(this);

		this._scrollableGroup = shadowRoot.querySelector('#scrollable-group');
		this._scrollableGroup.addEventListener('scroll', this._dispatchMove);
		window.addEventListener('resize', () => this._dispatchMove(null));

		this._contentGroup = shadowRoot.querySelector('#content');

		this._moveableGroup = shadowRoot.querySelector('moveable-group');
		this._moveableGroup.target = this;
		this._moveableGroup.addEventListener('tap', async event => {
			await this._toggleFinished;
			this._active = !this._active;
			if (!this._active) {
				this._scrollableGroup.scrollTo(this.contentLength, this.contentLength);
				await new Promise(resolve => setTimeout(resolve, 50));
			}
			const axis = this.isHorizontal ? 'offsetX' : 'offsetY';
			const options = { [axis]: this._active ? 0 : -this.slideLength, duration: TRANSITION_SPEED };
			this._toggleFinished = this._moveableGroup.move(options);
			this._dispatchMove(event, options);
		});
		this._moveableGroup.addEventListener('move', event => {
			const axis = this.isHorizontal ? 'offsetX' : 'offsetY';
			event.target[axis] = this.active ? 0 : -this.slideLength;
			this._dispatchMove(event);
		});
	}

	async connectedCallback() {
		await nextTick();
		const axis = this.isHorizontal ? 'offsetX' : 'offsetY';
		this._moveableGroup[axis] = this.active ? 0 : -this.slideLength;
		await nextTick();
		this._captureOffset();
	}

	_captureOffset({ offsetX, offsetY } = {}) {
		this._offsetXLast = offsetX ?? this.offsetX;
		this._offsetYLast = offsetY ?? this.offsetY;
		this._scrollTopLast = this.scrollTop;
		this._scrollLeftLast = this.scrollLeft;
	}

	get isHorizontal() {
		return window.innerWidth < window.innerHeight;
	}

	get offsetX() { return this._moveableGroup.offsetX; }
	set offsetX(value) { this._moveableGroup.offsetX = value; }
	get offsetY() { return this._moveableGroup.offsetY; }
	set offsetY(value) { this._moveableGroup.offsetY = value; }
	get scrollTop() { return this._scrollableGroup.scrollTop; }
	get scrollLeft() { return this._scrollableGroup.scrollLeft; }
	get contentLength() { return this.isHorizontal ?
			this._contentGroup.clientWidth :
			this._contentGroup.clientHeight;
	}
	set contentLength(value) {
		this.style.setProperty('--piece-drawer-length', typeof value === 'number' ? `${value}px` : value);
	}

	get active() { return this._active; }
	set active(value) {
		if (!value) { this._scrollableGroup.scrollTo(this.contentLength, this.contentLength); }
		this._active = value;
		const axis = this.isHorizontal ? 'offsetX' : 'offsetY';
		this._moveableGroup[axis] = value ? 0 : -this.slideLength;
		nextTick().then(() => {
			this._captureOffset();
			this._dispatchMove(null);
		});
	}

	get slideLength() {
		const { width, height } = this.parentElement.getBoundingClientRect();
		const offset = this.isHorizontal ? width : height;
		return offset - HANDLE_SIZE_PX;
	}

	_dispatchMove(event, { duration = 0, offsetX, offsetY } = {}) {
		if (event) { event.stopPropagation(); }
		if (!this.isConnected) { return; }
		const detail = {
			duration,
			deltaX: (offsetX ?? this.offsetX) - this._offsetXLast - this.scrollLeft + this._scrollLeftLast,
			deltaY: (offsetY ?? this.offsetY) - this._offsetYLast - this.scrollTop + this._scrollTopLast,
		};
		this._captureOffset({ offsetX, offsetY });
		this.dispatchEvent(new CustomEvent('move', { detail, bubbles: true, composed: true }));
	}
}

function nextTick() {
	return new Promise(resolve => setTimeout(resolve, 0));
}

customElements.define('piece-drawer', PieceDrawer);
