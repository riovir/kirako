import { css } from '../../core/index.js';

export const styles = css`
	#playground {
		touch-action: none;
	}

	.puzzle-container {
		position: relative;
		background: var(--color-bg-subtle);
		border-radius: var(--spacer-radius);
		width: 100%;
		height: 100%;
		max-width: 100%;
		max-height: 100%;
		overflow: hidden;
	}

	.has-focus-on-unfinished [disabled] {
		opacity: 0.05;
	}

	@media screen and (max-width: 1023px) {
		kr-navbar:not([active]) {
			--color-bg-subtle: transparent;
		}
	}
`;
