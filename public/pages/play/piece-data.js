export function isInDrawer(piece) {
	return 'drawerIndex' in piece.dataset;
}

export function drawerIndexOf(piece) {
	if (!isInDrawer(piece)) { return; }
	return parseInt(piece.dataset.drawerIndex);
}

export function assocDrawerIndex(value, piece) {
	if (value === null || value === undefined) {
		delete piece.dataset.drawerIndex;
	}
	else {
		piece.dataset.drawerIndex = value;
	}
	return piece;
}
