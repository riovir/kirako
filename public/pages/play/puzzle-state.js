import { isInDrawer, drawerIndexOf } from './piece-data.js';

const Codes = Object.freeze({ IN_DRAWER: 'd' });

export function reflectLocation({ params, board }) {
	return ({ target: piece }) => {
		const reflect = isInDrawer(piece) ? reflectDrawerIndex : reflectPosition;
		reflect({ board, piece, params });
	};
}

function reflectPosition({ piece, params, board }) {
	const [x, y] = toXyUnitIn(board)([
		piece.offsetX || 0,
		piece.offsetY || 0,
	]);
	const rotation = piece.rotation % 360 || 0;
	const value = `${x}_${y}_${rotation % 360}`;
	params[piece.id] = value === '0_0_0' ? null : value;
}

function reflectDrawerIndex({ piece, params }) {
	const rotation = piece.rotation % 360 || 0;
	params[piece.id] = `${Codes.IN_DRAWER}_${drawerIndexOf(piece)}_${rotation}`;
}

export function applyLocation({ board, params, drawerAdd }) {
	for (const piece of board.querySelectorAll('moveable-group')) {
		const value = params[piece.id];
		if (!value) { continue; }
		const apply = value.startsWith(`${Codes.IN_DRAWER}_`) ? applyDrawer({ drawerAdd }) : applyPosition;
		apply({ board, piece, value });
	}
}

function applyPosition({ board, piece, value }) {
	const [x, y, rotation = 0] = value.split('_').map(number => parseInt(number));
	const [offsetX, offsetY] = fromXyUnitIn(board)([x, y]);
	Object.assign(piece, { offsetX, offsetY, rotation });
}

function applyDrawer({ drawerAdd }) {
	return ({ piece, value }) => {
		const [/* code */, index, rotation = 0] = value.split('_').map(number => parseInt(number));
		drawerAdd(piece, { index });
		Object.assign(piece, { rotation });
	};
}

function toXyUnitIn(board) {
	const { clientWidth, clientHeight } = board;
	const toUnit = (maxPx, px) => Math.round(px / maxPx * 1000);
	return ([x, y]) => [toUnit(clientWidth, x), toUnit(clientHeight, y)];
}

function fromXyUnitIn(board) {
	const { clientWidth, clientHeight } = board;
	const fromUnit = (maxUnit, unit) => Math.round(unit * maxUnit / 1000);
	return ([x, y]) => [fromUnit(clientWidth, x), fromUnit(clientHeight, y)];
}
