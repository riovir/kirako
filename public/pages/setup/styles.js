import { css } from '../../core/index.js';

export const styles = css`
	#playground {
		overflow-y: auto;
	}

	.setup-layout {
		display: grid;
		height: 100%;
		justify-self: center;
		align-self: flex-start;
		gap: var(--spacer-2x);
		grid-template-columns: min-content minmax(10ch, 1fr) min-content;
		grid-template-rows: repeat(7, auto) minmax(64px, 1fr) clamp(36px, 10vh, 180px) auto;
		align-items: center;
	}

	.setup-layout > input:not(.has-addon) {
		grid-column: span 2;
	}

	.input-addon-layout {
		display: grid;
		grid-template-columns: minmax(10ch, 1fr) auto;
		gap: var(--spacer-2x);
	}

	.setup-layout #puzzle-image,
	.setup-layout #pieces,
	.setup-layout #buttons,
	.setup-layout #gallery {
		grid-column: span 3;
	}

	.setup-layout label {
		white-space: nowrap;
	}

	#gallery {
		height: 100%;
		max-width: 100%;
		overflow-x: auto;
		display: flex;
		gap: var(--spacer-2x);
		padding: var(--spacer-1x) 0;
	}

	#buttons {
		display: grid;
		gap: var(--spacer-2x);
		grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
	}

	details {
		grid-column: span 3;
		width: 100%;
	}
	.export-layout {
		display: grid;
		grid-template-columns: 1fr 1fr 1fr;
		gap: var(--spacer-2x);
	}
	.export-layout > * {
		box-sizing: border-box;
		width: 100%;
	}
	.export-layout > textarea {
		grid-column: span 3;
		resize: none;
	}

	label {
		display: flex;
		align-items: center;
		gap: var(--spacer-2x);
	}

	input[type=checkbox] {
		justify-self: start;
	}

	.icon {
		height: 1em;
		transform: translateY(-0.125em);
	}

	.image {
		height: 0;
		min-height: 100%;
		object-fit: contain;
		object-position: top left;
	}

	.button.is-image {
		border: none;
		position: relative;
		padding: 0;
		line-height: 0;
		width: auto;
		height: 100%;
		overflow: hidden;
		min-width: max-content;
	}
	.button.is-image > .button {
		position: absolute;
		top: var(--spacer-2x);
		right: var(--spacer-2x);
	}

	.is-edit-action {
		display: none;
		pointer-events: all;
		background: rgba(0, 0, 0, 0.5);
		color: white;
		box-shadow: none;
		position: absolute;
		align-items: center;
		justify-content: center;
		padding: 0;
		top: var(--spacer-2x);
		left: var(--spacer-2x);
		right: var(--spacer-2x);
		bottom: var(--spacer-2x);
	}
	.is-edit-action svg {
		width: minmax(var(--spacer-4x), 80%);
		height: var(--spacer-4x);
	}
	[data-gallery-edit-active] .is-edit-action {
		display: flex;
	}
	@media (hover: hover) {
		.is-edit-action:hover {
			color: var(--color-fg-primary-emphasis);
		}
		[data-gallery-edit-active] .button.is-image:hover {
			box-shadow: none;
			border: none;
		}
	}
	[data-gallery-edit-active] .button.is-image .image {
		min-width: 72px;
	}
	[data-gallery-edit-active] #gallery-edit-button {
		background: var(--color-bg-primary-soft);
		border-color: var(--color-border-primary-default);
		color: var(--color-fg-primary-emphasis);
	}
`;
