import '../play/index.js';
import './pieces-input.js';
import { downloadFile, uploadFile } from './gallery-io.js';

export const DEFAULT_IMAGE = 'assets/crafty-bot.jpg';

export function setup({ playground, params, storage }) {
	playground.innerHTML = /* html */`
		<div class="setup-layout">
			<label id="search-image-label">Search ArtStation</label>
			<kr-combo-box id="search-image" aria-labelledby="search-image-label" class="has-addon">
				<option>Car</option>
				<option>Motorcycle</option>
				<option>Fantasy</option>
				<option>Coffee</option>
				<option>Dog</option>
				<option>Space</option>
			</kr-combo-box>
			<button id="search-image-button" class="button" tabindex="-1">Search</button>

			<label for="image-url">Puzzle image URL</label>
			<input id="image-url" class="input has-addon" type="url" pattern="https://.*" autocomplete="off">
			<button id="image-clear-button" class="button">Clear</button>

			<pieces-input id="pieces"></pieces-input>

			<label for="pieces-scrambled">Scramble pieces</label>
			<input id="pieces-scrambled" class="input" type="checkbox">

			<label for="pieces-rotated">Rotate pieces</label>
			<input id="pieces-rotated" class="input" type="checkbox">

			<label for="match-orientation">Match orientation</label>
			<input id="match-orientation" class="input" type="checkbox">

			<details>
				<summary>Import / export gallery</summary>
				<div class="export-layout">
					<textarea id="gallery-input" class="input is-scrollable" rows="6"></textarea>
					<kr-button id="gallery-download-button" class="button">Download</kr-button>
					<kr-button id="gallery-export-button" class="button">Export</kr-button>
					<kr-button id="gallery-import-button" class="button">Import</kr-button>
				</div>
			</details>

			<orientable-image id="puzzle-image" class="image"></orientable-image>

			<div id="gallery" class="is-scrollable"></div>

			<div id="buttons">
				<button id="gallery-edit-button" class="button">Edit Gallery</button>
				<button id="share-button" class="button">Share</button>
				<button id="play-button" class="button">Play</button>
			</div>
		</div>
	`;

	const gallery = playground.querySelector('#gallery');
	const galleryInput = playground.querySelector('#gallery-input');
	const galleryEditButton = playground.querySelector('#gallery-edit-button');
	const galleryExportButton = playground.querySelector('#gallery-export-button');
	const galleryImportButton = playground.querySelector('#gallery-import-button');
	const galleryDownloadButton = playground.querySelector('#gallery-download-button');
	const imageClearButton = playground.querySelector('#image-clear-button');
	const imageInput = playground.querySelector('#image-url');
	const matchOrientationInput = playground.querySelector('#match-orientation');
	const piecesRotatedInput = playground.querySelector('#pieces-rotated');
	const piecesScrambledInput = playground.querySelector('#pieces-scrambled');
	const playButton = playground.querySelector('#play-button');
	const piecesInput = playground.querySelector('#pieces');
	const puzzleImage = playground.querySelector('#puzzle-image');
	const searchUnsplashInput = playground.querySelector('#search-image');
	const searchUnsplashButton = playground.querySelector('#search-image-button');
	const shareButton = playground.querySelector('#share-button');

	const croppings = storage.croppings || {};
	const updateImage = async () => {
		const src = params.src || DEFAULT_IMAGE;
		storage.currentImage = src;
		const cropping = croppings[src];
		Object.assign(puzzleImage, { src, cropping });
		await puzzleImage.updated;
	};
	updateImage();

	searchUnsplashInput.addEventListener('submit', openUnsplash);
	searchUnsplashButton.addEventListener('click', () => searchUnsplashInput.submit());

	imageInput.value = params.src || '';
	imageInput.addEventListener('input', ({ target }) => {
		params.src = target.value;
		updateImage();
	});
	imageClearButton.addEventListener('click', () => {
		update({ input: imageInput, value: '' });
	});

	piecesScrambledInput.checked = params.scramble ?? storage.scramble ?? true;
	params.scramble = piecesScrambledInput.checked;
	piecesScrambledInput.addEventListener('change', ({ target }) => {
		params.scramble = target.checked;
		storage.scramble = target.checked;
	});

	piecesRotatedInput.checked = params.rotate ?? storage.rotate ?? false;
	params.rotate = piecesRotatedInput.checked;
	piecesRotatedInput.addEventListener('change', ({ target }) => {
		params.rotate = target.checked;
		storage.rotate = target.checked;
	});

	const updateOrientation = () => {
		storage.matchOrientation = matchOrientationInput.checked;
		const getOrientation = () => playground.clientWidth < playground.clientHeight ? 'portrait' : 'landscape';
		const orientation = !matchOrientationInput.checked ? null : getOrientation();
		params.orientPuzzle = orientation;
		puzzleImage.orientation = orientation;
	};
	matchOrientationInput.checked = !!storage.matchOrientation;
	matchOrientationInput.addEventListener('change', updateOrientation);
	updateOrientation();

	puzzleImage.addEventListener('load', rememberImage({ storage }));

	const importAll = async urls => {
		const knownUrls = storage.puzzleImages;
		const newEntries = await Promise.all(urls
				.map(line => line.trim())
				.filter(Boolean)
				.filter(url => !knownUrls.includes(url))
				.map(tryLoading));
		const newUrls = newEntries.filter(Boolean);
		galleryInput.value = '';
		if (!newUrls.length) { return; }
		storage.puzzleImages = [...newUrls, ...knownUrls];
		location.reload();
	}
	galleryImportButton.addEventListener('click', () => {
		const toUrls = text => text.split(/\s/gm).filter(Boolean);
		const urls = toUrls(galleryInput.value);
		if (urls.length) {
			return importAll(urls);
		}
		const onLoad = content => importAll(toUrls(content));
		uploadFile({ onLoad });
	});
	galleryExportButton.addEventListener('click', () => {
		galleryInput.value = storage.puzzleImages.join('\n');
		galleryInput.select();
	});
	galleryDownloadButton.addEventListener('click', () => downloadFile({
		name: 'puzzle-gallery.kirako',
		content: storage.puzzleImages.join('\n'),
	}));

	galleryEditButton.addEventListener('click', () => playground.toggleAttribute('data-gallery-edit-active'));

	if (!navigator.share) { shareButton.style.display = 'none'; }
	shareButton.addEventListener('click', () => {
		navigator.share({
			title: 'Glitchy Play',
			text: `A ${params.piecesX}x${params.piecesY} puzzle`,
			url: location.href.replace('setup/index.js', 'play/index.js'),
		});
	});

	playButton.addEventListener('click', async () => {
		const { piecesX, piecesY } = piecesInput;
		Object.assign(params, { piecesX, piecesY });
		params.seed = params.seed ?? String(Math.random()).slice(2);
		await new Promise(resolve => setTimeout(resolve, 100));
		location.hash = location.hash.replace('setup/index.js', 'play/index.js');
	});

	(storage.puzzleImages ?? []).forEach(src => {
		const { button, removeButton } = ImageButton({ src, cropping: croppings[src] });
		button.addEventListener('click', () => {
			update({ input: imageInput, value: src });
		});
		removeButton.addEventListener('click', event => {
			event.stopPropagation();
			forgetImage({ src, storage });
			update({ input: imageInput, value: '' });
			button.remove();
		});
		gallery.appendChild(button);
	});

	piecesInput.setBoardElement(playground);
	window.addEventListener('resize', () => piecesInput.setBoardElement(playground));
	Object.assign(piecesInput, {
		pieceMinSizePx: storage.pieceMinSizePx ?? 48,
		piecesPreferred: storage.piecesPreferred ?? 10,
	})
	piecesInput.addEventListener('pieces-changed', ({ target: { pieceMinSizePx, piecesPreferred } }) => {
		Object.assign(storage, { pieceMinSizePx, piecesPreferred });
	});
	puzzleImage.addEventListener('load', ({ target }) => piecesInput.setPuzzleImage(target));
}

function openUnsplash({ target: { value } }) {
	if (!value) { return; }
	const search = encodeURIComponent(value.toLowerCase().trim());
	open(`https://www.artstation.com/search?query=${search}`, '_blank').focus();
}

export function rememberImage({ storage }) {
	return ({ target: { src } }) => {
		if (typeof src !== 'string') { return; }
		const images = storage.puzzleImages ?? [];
		const isNotDefault = src => !src.endsWith(DEFAULT_IMAGE);
		storage.puzzleImages = [... new Set([src, ...images])]
				.filter(isNotDefault);
	};
}

function forgetImage({ storage, src }) {
	storage.puzzleImages = storage.puzzleImages.filter(value => value !== src);
	navigator.serviceWorker?.controller?.postMessage?.({ type: 'forget-image', src });
}

function ImageButton({ src = '', alt = '', cropping } = {}) {
	const button = document.createElement('button');
	button.className = 'button is-image';
	const image = document.createElement('croppable-image');
	image.className = 'image';
	button.appendChild(image);

	const removeButton = document.createElement('button');
	removeButton.classList = 'button is-edit-action';
	removeButton.setAttribute('aria-label', 'Remove image')
	removeButton.innerHTML = /* html */`
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width="100%" height="100%" focusable="false" aria-hidden="true">
			<path fill="currentColor" d="M 0 1 l 1 -1 l 4 4 l 4 -4 l 1 1 l -4 4 l 4 4 l -1 1 l -4 -4 l -4 4 l -1 -1 l 4 -4 z" />
		</svg>
	`;
	image.addEventListener('load', () => {
		const { naturalWidth, naturalHeight } = image;
		button.style.aspectRatio = `${naturalWidth} / ${naturalHeight}`;
	});
	button.appendChild(removeButton);

	Object.assign(image, { src, alt, cropping });
	return { button, image, removeButton };
}

function update({ input, value }) {
	if (input.value === value) { return; }
	input.value = value;
	input.dispatchEvent(new Event('input'));
}

export function tryLoading(src) {
	return new Promise(resolve => {
		const img = document.createElement('img');
		img.onload = () => resolve(src);
		img.onerror = () => resolve(null);
		img.setAttribute('rel', 'preload');
		img.src = src;
	});
}
