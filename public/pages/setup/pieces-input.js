const SVG_PIECE = /* html */`
<svg viewbox="0 0 1.3 1.15" width="100%">
	<path fill="currentColor" d="M 0.15 0.068 c 0.2 0.1 0.42 -0.07 0.42 0 c 0 0.07 -0.14 0.15 0.08 0.15 c 0.22 0 0.07 -0.08 0.07 -0.15 c 0 -0.075 0.2 -0.1 0.43 0 c -0.1 0.2 -0.07 0.42 0 0.42 c 0.07 0 0.15 -0.14 0.15 0.08 c 0 0.22 -0.08 0.07 -0.15 0.07 c -0.075 0 0.1 0.2 0 0.43 c -0.2 -0.1 -0.42 0.07 -0.42 0 c 0 -0.07 0.14 -0.15 -0.08 -0.15 c -0.22 0 -0.07 0.08 -0.07 0.15 c 0 0.075 -0.2 0.1 -0.43 0 c 0.1 -0.2 0.07 -0.42 0 -0.42 c -0.07 0 -0.15 0.14 -0.15 -0.08 c 0 -0.22 0.08 -0.07 0.15 -0.07 c 0.075 0 -0.1 -0.2 0 -0.43 Z"></path>
</svg>
`;

const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		display: block;
		min-width: 220px;
	}

	.field,
	.inputs {
		display: inline-grid;
	}

	.inputs {
		gap: var(--spacer-2x);
		background: var(--color-bg-subtle);
		padding: var(--spacer-2x) var(--spacer-4x);
		border-radius: var(--spacer-radius);
	}

	.piece {
		display: grid;
		align-items: center;
		justify-items: center;
		color: var(--color-bg-soft);
		width: var(--pieces_input-piece-size);
	}
	.piece > * {
		grid-area: 1 / 1;
	}
	#min-size-label {
		color: var(--color-fg-muted);
		font-weight: bold;
		line-height: 1;
		font-size: calc(var(--pieces_input-piece-size) * 0.2);
	}
	#min-size {
		color: var(--color-fg-default);
	}
</style>
<details>
	<summary id="summary"></summary>
	<div class="inputs">
		<div class="field">
			<label for="preferred">Pieces preferred</label>
			<input id="preferred" type="number" />
		</div>
		<div class="field">
			<label for="min-size">Minimum piece size</label>
			<input id="min-size" type="range" min="24" value="48" step="8" max="128" />
		</div>
		<div class="piece">
			${SVG_PIECE}
			<span id="min-size-label"></span>
		</div>
	</div>
</details>
`;

class PiecesInput extends HTMLElement {
	_aspectRatio = 1;
	_boardElement = undefined;
	_boardHeightPx = Infinity;
	_boardWidthPx = Infinity;
	_piecesX = 0;
	_piecesY = 0;

	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this._minSizeInput = shadowRoot.querySelector('#min-size');
		this._piecesPreferredInput = shadowRoot.querySelector('#preferred');
		this._summaryElement = shadowRoot.querySelector('#summary');
		this._minSizeElement = shadowRoot.querySelector('#min-size-label');

		this._updatePieces = this._updatePieces.bind(this);

		this._updatePieces();
		this._minSizeInput.addEventListener('input', this._updatePieces);
		this._piecesPreferredInput.addEventListener('input', this._updatePieces);
	}

	connectedCallback() {
		this.piecesPreferred ||= 4;
		this.pieceMinSizePx ||= 48;
	}

	set aspectRatio(value) {
		const number = Number(value);
		if (!number) { return; }
		this._aspectRatio = number;
		this._updatePieces();
	}
	get aspectRatio() {
		return this._aspectRatio;
	}

	set boardHeightPx(value) {
		const number = Number(value);
		if (!number) { return; }
		this._boardHeightPx = number;
		this._updatePieces();
	}
	get boardHeightPx() {
		return this._boardElement?.offsetHeight ?? this._boardHeightPx;
	}

	set boardWidthPx(value) {
		const number = Number(value);
		if (!number) { return; }
		this._boardWidthPx = number;
		this._updatePieces();
	}
	get boardWidthPx() {
		return this._boardElement?.offsetWidth ?? this._boardWidthPx;
	}

	set pieceMinSizePx(value) {
		const number = Number(value);
		if (!number) { return; }
		this._minSizeInput.value = number;
		this._updatePieces();
	}
	get pieceMinSizePx() {
		return parseInt(this._minSizeInput.value);
	}

	set piecesPreferred(value) {
		const number = Number(value);
		if (!number) { return; }
		this._piecesPreferredInput.value = number;
		this._updatePieces();
	}
	get piecesPreferred() {
		return parseInt(this._piecesPreferredInput.value);
	}

	get piecesX() { return this._piecesX; }
	get piecesY() { return this._piecesY; }

	setBoardElement(el) {
		const { offsetWidth, offsetHeight } = el;
		if (!offsetWidth || !offsetHeight) { return; }
		this._boardElement = el;
		this.boardWidthPx = offsetWidth;
		this.boardHeightPx = offsetHeight;
	}

	setPuzzleImage({ naturalWidth, naturalHeight }) {
		this.aspectRatio = naturalWidth / naturalHeight;
	}

	_updateSummary() {
		this._summaryElement.textContent = `Puzzle size: ${this.piecesX} x ${this.piecesY}`;
	}

	_updatePieces() {
		const { aspectRatio, boardWidthPx, boardHeightPx, piecesPreferred, pieceMinSizePx } = this;
		const piecesXMax = Math.floor(boardWidthPx / pieceMinSizePx);
		const piecesYMax = Math.floor(boardHeightPx / pieceMinSizePx);
		const { piecesX, piecesY } = calculatePieces({ aspectRatio, piecesXMax, piecesYMax, piecesPreferred });
		if (!piecesX || !piecesY) { return; }
		this._piecesX = piecesX;
		this._piecesY = piecesY;
		this._updateSummary();
		this.style.setProperty('--pieces_input-piece-size', `${this.pieceMinSizePx * 1.3}px`);
		this._minSizeElement.textContent = `${this.pieceMinSizePx}px`;
		this.dispatchEvent(new Event('pieces-changed', { bubbles: true, composed: true }));
	}
}

customElements.define('pieces-input', PiecesInput);

export function calculatePieces({ aspectRatio = 1, piecesXMax = Infinity, piecesYMax = Infinity, piecesPreferred = 1 }) {
	if (!piecesPreferred) { return {}; }
	let option = { x: 1, y: 1, overPreference: piecesPreferred < 1 };
	let options = [option];

	const nextOption = ([{ x, y }]) => {
		const option = x / y < aspectRatio ? { x: x + 1, y } : { x, y: y + 1 };
		if (piecesXMax < option.x || piecesYMax < option.y) { return null; }
		const overPreference = piecesPreferred < option.x * option.y;
		return { ...option, overPreference };
	};

	while(option && !options[0].overPreference) {
		options = [option, options[0]];
		option = nextOption(options);
	}
	const getShapeMismatch = ({ x, y }) => Math.abs(1 - x / y / aspectRatio);
	const [optionA, optionB] = options;
	const mismatchA = getShapeMismatch(optionA);
	const mismatchB = getShapeMismatch(optionB);
	const { x, y } = mismatchA < mismatchB ? optionA : optionB;

	return { piecesX: x, piecesY: y };
}
