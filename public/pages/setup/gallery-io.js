export function downloadFile({ name, content }) {
	const anchor = document.createElement('a');
	anchor.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(content)}`);
	anchor.setAttribute('download', name);

	anchor.style.display = 'none';
	document.body.appendChild(anchor);

	anchor.click();

	document.body.removeChild(anchor);
}

export function uploadFile({ onLoad }) {
	const upload = document.createElement('input');
	upload.setAttribute('type', 'file');
	upload.setAttribute('accept', '.txt, .kirako');
	upload.addEventListener('change', async ({ target }) => {
		if (!target.files?.length) { return; }
		const [file] = target.files;
		return readFile(file).then(onLoad);
	});

	upload.style.display = 'none';
	document.body.appendChild(upload);

	upload.click();

	document.body.removeChild(upload);
}

function readFile(file) {
	return new Promise(resolve => {
		const reader = new FileReader();
		reader.addEventListener('load', ({ target }) => resolve(target.result));
		reader.readAsText(file);
	});
}
