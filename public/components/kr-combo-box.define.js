const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		box-sizing: border-box;
		position: relative;
		display: flex;
		width: 100%;
	}
	:host * {
		box-sizing: inherit;
	}

	#options {
		position: absolute;
		width: 100%;
		min-width: min-content;
		left: 0;
		top: calc(var(--kr-combo-box-input-height, 32px) + var(--spacer-6x) + var(--spacer-1x));
		padding: var(--spacer-2x);
		border-radius: var(--spacer-radius);
		background: var(--color-bg-subtle);
		box-shadow: var(--shadow-2);
		border: none;
		gap: var(--spacer-2x);
		flex-wrap: wrap;
		grid-template-columns: repeat(auto-fill, minmax(4em, 1fr));
		grid-template-columns: repeat(auto-fill, minmax(0, max-content));
		z-index: var(--z-topmost);
	}
	#options[open] {
		display: flex;
	}
	#options > * {
		flex-grow: 1;
	}
	#options::before {
		position: absolute;
		top: calc(-1 * var(--spacer-4x));
		left: var(--spacer-4x);
		display: block;
		content: '';
		width: 0;
		height: 0;
		border-left: var(--spacer-4x) solid transparent;
		border-right: var(--spacer-4x) solid transparent;
		border-bottom: var(--spacer-4x) solid var(--color-bg-subtle);
		z-index: var(--z-topmost);
	}

	input {
		flex-grow: 1;
		min-height: 42px;
		min-width: 4em;
	}

	input,
	button {
		font-size: 1em;
		border: 1px solid var(--color-border-default);
		padding: 0.5em 1em;
		border-radius: var(--spacer-radius);
		cursor: pointer;
	}
	button {
		color: var(--color-fg-default);
		background: var(--color-bg-default);
	}
	@media (hover: hover) {
		input:hover,
		button:hover {
			border-color: var(--color-border-primary-default);
			box-shadow: 0px 0px 0px var(--spacer-1x) var(--color-bg-primary-soft);
		}
	}
	button:active {
		background: var(--color-bg-subtle);
	}
</style>

<input id="input" type="search" autocomplete="off" />
<dialog id="options"></dialog>
<div hidden><slot></slot></div>
`;

export class KrComboBox extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this.submit = this.submit.bind(this);

		this._inputElement = shadowRoot.querySelector('#input');
		this._optionsDialog = shadowRoot.querySelector('#options');

		this._renderOptions = this._renderOptions.bind(this);
		this._showOptions = this._showOptions.bind(this);
		this._closeOptions = this._closeOptions.bind(this);
		this._onKeyDown = this._onKeyDown.bind(this);

		this._inputElement.addEventListener('click', this._showOptions);
		this.addEventListener('keydown', this._onKeyDown);
		this.addEventListener('blur', this._closeOptions);

		this._observer = new MutationObserver(this._renderOptions);
		shadowRoot.addEventListener('slotchange', this._renderOptions);
	}

	connectedCallback() {
		this.setAttribute('role', 'textbox');
		this._renderOptions();
		this._observer.observe(this, { childList: true, subtree: true, characterData: true });
	}

	get options() {
		const isOption = ({ value, textContent }) => value || textContent;
		return filter(isOption, this.querySelectorAll(':scope > :not([slot])'));
	}

	get value() {
		return this._inputElement.value;
	}
	set value(value) {
		this._inputElement.value = value;
		this.dispatchEvent(new Event('change', { bubbles: true, composed: true }));
	}

	_showOptions() {
		if (!this.options.length) { return; }
		this.style.setProperty('--kr-combo-box-input-height', `${this._inputElement.clientHeight}px`);
		this._optionsDialog.open = true;
	}

	_closeOptions(event) {
		if (this.contains(event?.relatedTarget)) { return; }
		this._optionsDialog.close?.();
	}

	_onKeyDown(event) {
		if (event.key === 'Tab') { return; }
		if (event.key === 'Enter') { return this.submit(); }
		event.stopPropagation();
		this._showOptions();
	}

	submit() {
		this.dispatchEvent(new Event('submit', { bubbles: true, composed: true }));
	}

	_renderOptions() {
		this._optionsDialog.innerHTML = '';
		this.options.forEach(option => {
			const button = document.createElement('button');
			button.textContent = option.textContent;
			button.addEventListener('click', () => {
				this.value = option.value;
				this._closeOptions();
				this.submit();
			});
			this._optionsDialog.appendChild(button);
		});
	}
}

customElements.define('kr-combo-box', KrComboBox);

/** Used as https://ramdajs.com/docs/#filter */
function filter(predicate, array) {
	const result = [];
	for (const element of array) {
		if (predicate(element)) {
			result.push(element);
		}
	}
	return result;
}
