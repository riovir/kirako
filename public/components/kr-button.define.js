const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		box-sizing: border-box;
		display: inline-flex;
		justify-content: center;
		align-items: center;
		font-size: 1em;
		padding: 0.5em 1em;
		min-width: 6em;
		min-height: 44px;
		border: 1px solid var(--color-border-default);
		border-radius: var(--spacer-radius);
		cursor: pointer;
		white-space: nowrap;
	}
	:host([hidden]) {
		display: none;
	}

	@media (hover: hover) {
		:host(:hover) {
			color: var(--color-fg-primary-emphasis);
			border-color: var(--color-border-primary-default);
			box-shadow: 0px 0px 0px var(--spacer-1x) var(--color-bg-primary-soft);
		}
	}

	:host(:focus-visible:not([disabled])) {
		color: var(--color-fg-primary-emphasis);
		border-color: var(--color-border-primary-default);
		box-shadow: 0px 0px 0px var(--spacer-1x) var(--color-bg-primary-soft);
		outline: 2px solid var(--color-border-primary-emphasis);
		outline-offset: 2px;
	}

	:host(:active),
	:host(.is-active:not([disabled])) {
		background: var(--color-bg-primary-soft);
		border-color: var(--color-border-primary-default);
		color: var(--color-fg-primary-emphasis);
	}

	::slotted(svg),
	::slotted(kr-icon),
	::slotted(.icon) {
		width: 1em;
		height: 1em;
	}

	/* Minimal */

	:host([minimal]:not(:active):not(.is-active)) {
		border-color: transparent;
		background: transparent;
	}
	:host([minimal]:not(:active):not(.is-active):focus-visible:not([disabled])) {
		border-color: var(--color-border-primary-default);
		background: var(--color-bg-default);
	}
	@media (hover: hover) {
		:host([minimal]:not(:hover):not(:active):not(.is-active)) {
			border-color: transparent;
			background: transparent;
		}
		:host([minimal]:hover:not(:active):not(.is-active)) {
			border-color: var(--color-border-primary-default);;
			background: var(--color-bg-default);
		}
	}

	/* Compact */

	:host([compact]) {
		aspect-ratio: 1;
		min-width: auto;
		padding: 0;
	}

	:host([disabled]),
	:host([aria-disabled='true']) {
		pointer-events: none;
		opacity: 0.6;
		cursor: default;
	}
</style>
<slot></slot>
`;

export class KrButton extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
	}

	connectedCallback() {
		this.setAttribute('aria-disabled', 'false');
		this.setAttribute('role', 'button');
		this.setAttribute('tabindex', '0');
		this.disabled = this.disabled;
	}

	get compact() {
		return this.hasAttribute('compact');
	}
	set compact(value) {
		if (value) { this.setAttribute('compact', ''); }
		else { this.removeAttribute('compact'); }
	}

	get minimal() {
		return this.hasAttribute('minimal');
	}
	set minimal(value) {
		if (value) { this.setAttribute('minimal', ''); }
		else { this.removeAttribute('minimal'); }
	}

	get disabled() {
		return this.hasAttribute('disabled');
	}
	set disabled(value) {
		this._setDisabled(value);
	}

	get ariaDisabled() {
		return this.getAttribute('aria-disabled');
	}
	set ariaDisabled(value) {
		this._setDisabled(value);
	}

	_setDisabled(value) {
		if (value) { this.setAttribute('disabled', ''); }
		else { this.removeAttribute('disabled'); }
		this.setAttribute('aria-disabled', value ? 'true' : 'false');
		this.setAttribute('tabindex', value ? -1 : 0);
	}
}

customElements.define('kr-button', KrButton);
