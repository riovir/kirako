import { KrButton } from './kr-button.define.js';

export default {
	title: 'KrButton',
	component: KrButton,
	args: { textContent: 'Button' },
}

export const Base = {};
export const Primary = { args: { minimal: true, textContent: 'Minimal' } };
