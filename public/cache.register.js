if ('serviceWorker' in navigator) {
	const registerWorker = () => {
		navigator.serviceWorker
			.register('cache.js')
			.then(() => { console.log('ServiceWorker registered'); })
			.catch(err => { console.error('ServiceWorker registration failed', err); });
	};
	window.addEventListener('load', registerWorker);
}
