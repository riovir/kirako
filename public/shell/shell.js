import { adoptStyleSheets } from '../core/index.js';
import { PageContext } from './page-context.js';

export function mount({ outlet, routes, defaultRoute = routes[0], location }) {
	const state = {
		snippetModule: null,
	};

	const context = PageContext({ location });
	globalThis.__debug = { context };

	const Elements = {
		navbar: outlet.querySelector('#navbar'),
		playground: outlet.querySelector('#playground'),
		attribution: outlet.querySelector('#attribution'),
		goFullscreen: outlet.querySelector('#go-fullscreen'),
	};

	routes.map(NavbarItem)
		.forEach(element => Elements.navbar.appendChild(element));
	Elements.navbar.appendChild(Elements.attribution.content.cloneNode(true));

	Elements.goFullscreen.addEventListener('click', requestFullscreen);
	if (!outlet.requestFullscreen && !outlet.webkitRequestFullscreen) { Elements.goFullscreen.style.display = 'none'; }

	const snippetUrl = routeOf(location.href) || defaultRoute;
	if (!location.hash) { location.hash = `#${snippetUrl}`; }

	loadRoute({ snippetUrl, context });
	window.addEventListener('hashchange', () => loadRoute({ context }), false);
	preloadRoutes();

	function loadRoute({ newURL = window.location.href, snippetUrl = routeOf(newURL), context }) {
		if (screen.orientation) { screen.orientation.onchange = null; }
		Elements.navbar.active = false;
		return import(`./${snippetUrl}`)
			.then(({ title = snippetUrl, setup, styles }) => {
				adoptStyleSheets(styles ? [styles] : [], document);
				state.snippetModule = { url: snippetUrl, title, setup };
				return state.snippetModule;
			})
			.then(renderSnippet)
			.then(setupCurrentSnippet({ context }));
	}

	async function preloadRoutes() {
		return Promise.all(routes.map(url => import(`../${url}`)));
	}

	function renderSnippet({ url, title = url }) {
		Elements.navbar = resetNavbar(Elements);
		Elements.playground = resetPlayground(Elements);

		document.title = title;
		Elements.navbar.querySelector(`a.is-active`)?.classList.remove('is-active');
		const [routeUrl] = location.hash.split('?');
		Elements.navbar.querySelector(`a[href="${routeUrl}"]`)?.classList.add('is-active');
	}

	function setupCurrentSnippet({ context }) {
		return async () => {
			const { navbar, playground } = Elements;
			await state.snippetModule?.setup?.({ navbar, playground, ...context });
		}
	};

	async function requestFullscreen() {
		if (outlet.requestFullscreen) {
			return outlet.requestFullscreen();
		}
		if (outlet.webkitRequestFullscreen) {s
			return outlet.webkitRequestFullscreen();
		}
	}
}

function resetNavbar({ navbar }) {
	for (const element of navbar.querySelectorAll('[slot=brand]:not([data-keep-on-reset])')) {
		element.remove();
	}
	return navbar;
}

function resetPlayground({ playground }) {
	const element = document.createElement('div');
	element.id = 'playground';
	playground.replaceWith(element);
	return element;
}

function NavbarItem(href) {
	const item = document.createElement('a')
	item.setAttribute('href', `#${href}`);
	item.setAttribute('slot', 'end');
	item.textContent = format(href);
	return item;
}

function format(url) {
	const isNotIndex = name => name !== 'index.js';
	const [file] = url.split('/').filter(isNotIndex).reverse();
	return file.split('-').join(' ').replace(/\.js$/, '');
}

function routeOf(href) {
	const { hash } = new URL(href);
	const [url] = hash.replace('#', '').split('?');
	return url ? `../${url}` : '';
}
