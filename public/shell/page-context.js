export function PageContext({ location }) {
	const params = QueryParams({ location });
	const storage = LocalStorage();
	return { storage, params };
}

function QueryParams({ location }) {
	const getLocation = () => {
		const [snippet, query = ''] = location.hash.split('?');
		const params = new URLSearchParams(query);
		return { snippet, params };
	};
	let cache = {};
	let pushStateId = null;
	window.addEventListener('hashchange', () => { cache = {} }, false);
	return new Proxy(getLocation, {
		get: (target, prop) => {
			if (!(prop in cache)) { return tryJsonParse(target().params.get(prop)); }
			return cache[prop] ?? undefined;
		},
		set: (target, prop, value) => {
			cache[prop] = value;

			if (pushStateId) clearTimeout(pushStateId);
			pushStateId = setTimeout(() => {
				const { snippet, params } = target();
				const initial = params.toString();
				Object.entries(cache).forEach(([prop, value]) => {
					params.set(prop, JSON.stringify(value));
					if (value === null || value === undefined) {
						params.delete(prop);
					}
				});
				const updated = params.toString();
				if (initial === updated) { return; }
				history.pushState(null, '', `${snippet}?${updated}`);
			}, 100);
			return true;
		},
	});
}

function tryJsonParse(value) {
	try {
		return JSON.parse(value);
	}
	catch {
		return value;
	}
}

function LocalStorage() {
	return new Proxy(localStorage, {
		get: (target, prop) => {
			try { return JSON.parse(target.getItem(prop)); }
			catch { return; }
		},
		set: (target, prop, value) => {
			target.setItem(prop, JSON.stringify(value));
			return true;
		},
	});
}
