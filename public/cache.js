const PRODUCTION_MODE = true; // Altered by the dev-server
const VERSION = 1;
const CACHE_NAME = PRODUCTION_MODE ? `my-cache-v${VERSION}` : `dev-cache-v${VERSION}`;

self.addEventListener('install', () => { self.skipWaiting(); });
if (PRODUCTION_MODE) {
	self.addEventListener('fetch', respondWith(staleWhileRevalidate));
}
self.addEventListener('activate', reloadAllTabs);
self.addEventListener('activate', waitUntil(deleteStaleCaches));
self.addEventListener('message', onMessage);

async function staleWhileRevalidate(event) {
	const { request } = event;

	if (!isSameOrigin(request)) {
		return cacheFirst(event);
	}
	const cache = await caches.open(CACHE_NAME);

	const cachedResponse = await cache.match(request);
	const networkResponsePromise = fetch(request).catch(() => null);

	if (request.method === 'GET') {
		event.waitUntil(update({ cache, request, response: networkResponsePromise }));
	}

	return cachedResponse || networkResponsePromise;
}

async function cacheFirst(event) {
	const cache = await caches.open(CACHE_NAME);
	const { request } = event;
	const cachedResponse = await cache.match(request);
	if (cachedResponse) { return cachedResponse; }

	const networkResponsePromise = fetch(request).catch(() => null);
	event.waitUntil(update({ cache, request, response: networkResponsePromise }));

	return networkResponsePromise;
}

async function update({ cache, request, response }) {
	const _response = await response;
	if (!_response) { return; }
	if (isOpaque(_response)) {
		await cache.put(request, _response.clone());
		return;
	}
	await cache.add(request, _response.clone());
}

function isOk({ status }) {
	return 200 <= status && status < 300;
}

function isOpaque({ type }) {
	return type === 'opaque';
}

function isPartial({ status }) {
	return status === 206;
}

function isSameOrigin({ url }) {
	try {
		const { origin } = new URL(url);
		return origin === self.location.origin;
	}
	catch(err) {
		console.error('Invalid URL', err, url);
		return false;
	}
}

async function deleteStaleCaches() {
	const cacheNames = await caches.keys();
	await Promise.all(cacheNames
		.filter(cacheName => cacheName !== CACHE_NAME)
		.map(cacheName => caches.delete(cacheName)));
}

async function reloadAllTabs() {
	const tabs = await self.clients.matchAll({ type: 'window' });
	tabs.forEach(tab => { tab.navigate(tab.url); });
}

function onMessage({ data }) {
	if (data.type === 'forget-image') {
		return forgetImage(data);
	}
}

async function forgetImage({ src }) {
	const cache = await caches.open(CACHE_NAME);
	cache.delete(src);
}

function waitUntil(fn) {
	return event => event.waitUntil(fn(event));
}

function respondWith(fn) {
	return event => event.respondWith(fn(event));
}
