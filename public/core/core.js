export function css(staticParts, dynamicParts) {
	if (dynamicParts) {
		throw new Error('Dynamic CSS templates are not supported');
	}
	const css = staticParts.join('');
	const style = document.createElement('style');
	style.innerHTML = css;
	style.setAttribute('data-adopted-stylesheet', '');
	return style;
}

export function adoptStyleSheets(sheets, { head }) {
	const isNotIncludedBy = array => element => !array.includes(element);
	const remove = element => element.remove();
	const appendTo = parent => element => parent.appendChild(element);

	Array.from(head.querySelectorAll('[data-adopted-stylesheet]'))
		.filter(isNotIncludedBy(sheets))
		.forEach(remove);
	sheets.forEach(appendTo(head));
}
