import './cache.register.js';
import './components/define.js';
import { defaultRoute, routes } from './router.config.js';
import { mount } from './shell/index.js';

const { location } = window;

mount({ outlet: document.body, routes, defaultRoute, location });

const updateVh = () => {
	const vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
};
updateVh();
window.addEventListener('resize', updateVh);
