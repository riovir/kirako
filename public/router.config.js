export const defaultRoute = 'pages/setup/index.js';

export const routes = [
	defaultRoute,
	'pages/crop/index.js',
	'pages/play/index.js',
];
