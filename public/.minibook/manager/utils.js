/** Used as https://ramdajs.com/docs/#propEq */
export function propEq(prop, value) {
	return object => object[prop] === value;
}

export function withListener(event, handler) {
	return el => {
		el.addEventListener(event, handler);
		return el;
	};
}
