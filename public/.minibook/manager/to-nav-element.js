export function toNavElement({ path, csf }) {
	const anchor = document.createElement('a');
	const href = `#${[path]}`;
	const textContent = csf.default.title ?? '[Title missing]';
	return Object.assign(anchor, { href, textContent });
}
