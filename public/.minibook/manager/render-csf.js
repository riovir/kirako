import { propEq } from './utils.js';

export function renderCsf({ title, outlet, modules }) {
	return ({ target: { href } }) => {
		const { csf } = findCsf({ href, modules });
		if (!csf) { return; }
		title.textContent = csf.default.title;
		outlet.innerHTML = '';
		outlet.appendChild(createDocs({ csf }));
	};
}

function findCsf({ href, modules }) {
	const [/* base url */, path] = href.split('#');
	return modules.find(propEq('path', path)) ?? {};
}

function createDocs({ csf }) {
	const wrapper = document.createElement('div');
	Object.assign(wrapper, { className: 'mb-docs' });
	const { default: meta, ...stories } = csf;
	const { component, render = renderOf({ component }), args = {} } = meta;
	const storyElements = Object.entries(stories)
		.map(([key, value]) => [key, { render, ...value, args: { ...args, ...value.args } }])
		.map(([name, story]) => createStory({ name, story }));

	storyElements.forEach(el => wrapper.appendChild(el));

	return wrapper;
}

function createStory({ name, story: { render, args } }) {
	const wrapper = document.createElement('div');
	Object.assign(wrapper, { className: 'mb-story' });

	const title = document.createElement('h2');
	Object.assign(title, { className: 'mb-story__title', textContent: name });
	wrapper.appendChild(title);

	wrapper.appendChild(render(args));
	return wrapper;
}

function renderOf({ component }) {
	return args => {
		const el = new component();
		return Object.assign(el, { ...args });
	};
}
