import main from '../main.js';
import { toNavElement } from './to-nav-element.js';
import { renderCsf } from './render-csf.js';
import { withListener } from './utils.js';

const Elements = {
	nav: document.querySelector('#mb-navigation'),
	outlet: document.querySelector('#mb-outlet'),
	title: document.querySelector('#mb-title'),
};

const modules = await loadCsfModules(main);
modules.map(toNavElement)
	.map(withListener('click', renderCsf({ ...Elements, modules })))
	.forEach(el => Elements.nav.appendChild(el));

async function loadCsfModules({ stories }) {
	const importStory = async path => ({ path, csf: await import(`../${path}`) });
	return Promise.all(stories.map(importStory));
}
