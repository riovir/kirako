export function expect(candidate) {
	return {
		toBe: value => throwOnFail(toBe(candidate, value)),
		toBeSame: value => throwOnFail(toBeSame(candidate, value)),
	};
}

function toBe(candidate, value) {
	return {
		pass: candidate === value,
		message: () => `Expected ${format(candidate)} to be ${format(value)}`,
	};
}

function toBeSame(candidate, value) {
	return {
		pass: JSON.stringify(candidate) === JSON.stringify(value),
		message: () => `Expected JSON.stringified ${format(candidate)} to be ${format(value)}`,
	};
}

function throwOnFail({ pass, message }) {
	if (pass) { return; }
	throw new Error(message());
}

function format(value) {
	if (!['object', 'string'].includes(typeof value)) {
		return value;
	}
	return JSON.stringify(value, null, 2);
}
