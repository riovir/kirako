import { setup, reportResults } from './tests.js';
import { View } from './view.js';

const view = View({ container: document.querySelector('#results'), console });
const { loadTests, runTests } = setup({
	basePath: '../pages/',
	globals: window,
});

const specs = [
	'play/jigsaw-cutout.spec.js',
	'play/use-update.spec.js',
	'setup/pieces-input.spec.js',
];

loadTests(specs)
	.then(runTests)
	.then(reportResults(view));
