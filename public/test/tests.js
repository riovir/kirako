import { expect } from './expect.js';

export function setup({ globals, basePath }) {
	return {
		async loadTests(urls) {
			const _urls = [...urls];
			const suites = {};

			while(_urls.length) {
				const url = _urls.shift();
				const { tests, test } = useTest(url);
				globals.test = test;
				await import(basePath.concat(url));
				suites[url] = tests.current;
			}

			delete globals.test;
			return suites;
		},

		async runTests(suites) {
			globals.expect = expect;
			const entries = await Promise.all(Object.entries(suites)
				.map(async ([url, tests]) => {
					const finishedTests = await Promise.all(tests.map(runTest));
					return [url, finishedTests];
				}));
			delete globals.expect;
			return Object.fromEntries(entries);
		}
	};
}

function useTest() {
	const tests = { current: [] };
	return {
		tests,
		test(message, test) {
			tests.current = [...tests.current, { message, test }];
		},
	};
}

export async function runTests(suites) {
	const entries = await Promise.all(Object.entries(suites)
		.map(async ([url, tests]) => {
			const finishedTests = await Promise.all(tests.map(runTest));
			return [url, finishedTests];
		}));
	return Object.fromEntries(entries);
}

async function runTest({ message, test }) {
	try {
		await test();
		return { message, test, passed: true }
	}
	catch (cause) {
		return { message, test, passed: false, cause };
	}
}

export function reportResults({ describe, pass, fail }) {
	return results => {
		reportSummary({ describe, pass, fail, results });
		for(const url of Object.keys(results).sort()) {
			describe(url);
			results[url].forEach(reportTest({ pass, fail }));
		}
	}
}

function reportTest({ pass, fail }) {
	return ({ message, passed, cause }) => {
		if (passed) {
			pass(message);
		}
		else {
			fail(message, cause)
		}
	}
}

function reportSummary({ describe, pass, fail, results }) {
	const tests = Object.values(results).flat();
	const passes = tests.filter(test => test.passed).length;
	describe('Summary');
	if (passes === tests.length) {
		pass(`Passed ${passes} out of ${tests.length} tests`);
	}
	else {
		fail(`Failed ${tests.length - passes} out of ${tests.length} tests`);
	}
}
