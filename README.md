# kirako

Kirako, Hungarian for jigsaw is a for-fun pet project. Allows users to play jigsaw puzzles with images, even gifs that they find on the net.

## Try it out

> It should be live here: https://kirako.riovir.com
