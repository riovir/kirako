import { serveDevSw } from './middleware/index.js';

export default {
	open: true,
	rootDir: './public',
	middleware: [serveDevSw],
};
